# A comparison of performance portability frameworks for PIC

This git repository contains the source codes of the work published in

Artigues, V., Kormann, K., Rampp, M., Reuter, K. Evaluation of performance
portability frameworks for the implementation of a particle‐in‐cell code.
Concurrency Computat Pract Exper. 2020;
https://doi.org/10.1002/cpe.5640

These source codes implement a subset of the algorithms published in

Kraus, M., Kormann, K., Morrison, P., Sonnendrücker, E. (2017). GEMPIC:
Geometric electromagnetic particle-in-cell methods.  Journal of Plasma Physics,
83(4), 905830401;
https://doi.org/10.1017/S002237781700040X

The repository is organized in different branches, where different branches
contain the code for different performance portability frameworks.

Please note that this is an experimental code for testing purposes
and provided *as is* (see 'LICENSE' for further details).
